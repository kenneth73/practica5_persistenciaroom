package cat.itb.practica5_persistenciaroom;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.practica5_persistenciaroom.database.Puntuacio;

public class MainActivityReciclerView extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<Puntuacio> puntuacions = MainActivity.puntuacioRepository.getAll();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_puntuacio_recycler);
        recyclerView = findViewById(R.id.puntuacio_list_fragment_recicler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager((linearLayoutManager));
        PuntuacioAdapter puntuacioAdapter = new PuntuacioAdapter(puntuacions);
        recyclerView.setAdapter(puntuacioAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_all_btn_menu:
                MainActivity.puntuacioRepository.deleteAll();
                finish();
                //refresh this activity
                startActivity(getIntent());
                return true;
            case R.id.play_btn_menu:
                Intent i = new Intent(MainActivityReciclerView.this, MainActivity1.class);
                startActivity(i);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
