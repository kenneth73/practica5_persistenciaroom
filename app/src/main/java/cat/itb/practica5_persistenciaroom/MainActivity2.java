package cat.itb.practica5_persistenciaroom;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import cat.itb.practica5_persistenciaroom.database.AppDatabase;
import cat.itb.practica5_persistenciaroom.database.Pregunta;
import cat.itb.practica5_persistenciaroom.database.PreguntaDAO;
import cat.itb.practica5_persistenciaroom.database.PreguntaRepository;
import cat.itb.practica5_persistenciaroom.database.Puntuacio;
import cat.itb.practica5_persistenciaroom.database.PuntuacioDAO;
import cat.itb.practica5_persistenciaroom.database.PuntuacioRepository;

public class MainActivity2 extends AppCompatActivity {

    static AppDatabase db;
    static PreguntaDAO preguntaDAO;
    static PuntuacioDAO puntuacioDAO;
    static PreguntaRepository preguntaRepository;
    static PuntuacioRepository puntuacioRepository;

    private TextView preguntaTextView;
    private TextView breadcrumbsTextView;
    private EditText responseEditText;
    private TextView timmerTextView;

    private TextView resultatTextView;
    private Button tornarBtn;

    private List<Pregunta> preguntas;
    private String nameUser;
    private GameController gameController;
    CountDownTimer counter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);
        // DATA BASE //
        db = AppDatabase.getInstance(this.getApplicationContext());
        preguntaDAO =db.preguntaDAO();
        puntuacioDAO = db.puntuacioDAO();

        preguntaRepository = new PreguntaRepository(preguntaDAO);
        puntuacioRepository = new PuntuacioRepository(puntuacioDAO);

        //Add questions
        int numPreguntes = preguntaRepository.getNumberQuestions();
        if (numPreguntes == 0) {
            insertAllQuestions();
        }
        //deleteAllQuestions();

        // VIEWS GAME //
        preguntaTextView = findViewById(R.id.pregunta_textview);
        breadcrumbsTextView = findViewById(R.id.breadcrumbs_textview);
        responseEditText = findViewById(R.id.response_edittext);
        timmerTextView = findViewById(R.id.timmer_textview);
        // VIEWS END GAME //
        resultatTextView = findViewById(R.id.resultat_text_view);
        tornarBtn = findViewById(R.id.tornar_btn);
        // VIEW TIMMER
        timmerTextView = findViewById(R.id.timmer_textview);
        //GET QUESTIONS TO DB
        preguntas = preguntaRepository.getQuestions(); //get five questions

        //timmer
        counter = new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                timmerTextView.setText("Seconds remaining: " + (millisUntilFinished / 1000));
            }

            public void onFinish() {
                String res = responseEditText.getText().toString();
                if (gameController.getCurrentIndex() != GameController.TOTAL_QUESTIONS - 1) {
                    gameController.checkResponse(gameController.getCurrentIndex(), res);
                    gameController.increaseIndex();
                    loadDataToView();
                    counter.start();
                } else {
                    gameController.checkResponse(gameController.getCurrentIndex(), res);
                    saveResult();
                    endGame();
                }
            }
        };

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            nameUser= bundle.getString("name");
            gameController = new GameController(nameUser, preguntas);
            loadDataToView();
            counter.start();

        } else {
            Toast.makeText(MainActivity2.this, "Error, bundle is null", Toast.LENGTH_SHORT).show();
        }

        responseEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {//enter=66
                    String res = responseEditText.getText().toString();
                    if (gameController.getCurrentIndex() != GameController.TOTAL_QUESTIONS - 1) {

                        if (!res.equals("")) {
                            gameController.checkResponse(gameController.getCurrentIndex(), res);
                            //String p = "Puntuacio = " + gameController.getScore();
                            //Toast.makeText(MainActivity2.this, p, Toast.LENGTH_SHORT).show();
                            gameController.increaseIndex();
                            loadDataToView();
                            counter.cancel();
                            counter.start();

                        } else {
                            Toast.makeText(MainActivity2.this, "La resposta no pot estar buida", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        gameController.checkResponse(gameController.getCurrentIndex(), res);
                        saveResult();
                        endGame();
                    }
                }
                return true;
            }
        });

        tornarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity2.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void loadDataToView() {
        preguntaTextView.setText(gameController.getCurrentQuestion().getEnunciat());
        breadcrumbsTextView.setText(gameController.getBreadCrumbs());
        responseEditText.setText("");
    }

    public void endGame() {
        preguntaTextView.setVisibility(View.INVISIBLE);
        breadcrumbsTextView.setVisibility(View.INVISIBLE);
        responseEditText.setVisibility(View.INVISIBLE);
        timmerTextView.setVisibility(View.INVISIBLE);

        String resultat = "PUNTUACIÓ FINAL: " + gameController.getScore();
        resultatTextView.setText(resultat);
        resultatTextView.setVisibility(View.VISIBLE);
        tornarBtn.setVisibility(View.VISIBLE);
        resetGame();


    }

    public void resetGame() {
        gameController.setCurrentIndex(0);
        gameController.setScore(0);
    }

    public void saveResult() {
        Puntuacio p = new Puntuacio(gameController.getNameUser(), gameController.getScore());
        puntuacioRepository.insert(p);
    }

    public void insertAllQuestions() {
        Pregunta p1 = new Pregunta("Qui es el president d'Espanya", "Sanchez");
        preguntaRepository.insert(p1);
        Pregunta p2 = new Pregunta("Qui es el millor jugador del mon?", "Messi");
        preguntaRepository.insert(p2);
        Pregunta p3 = new Pregunta("Arrel quadrada de 144", "12");
        preguntaRepository.insert(p3);
        Pregunta p4 = new Pregunta("Velocitat de la llum(m/s)", "299792458");
        preguntaRepository.insert(p4);
        Pregunta p5 = new Pregunta(" ¿Cuál fue la primera película de Walt Disney?", "Blancanieves");
        preguntaRepository.insert(p5);
        Pregunta p6 = new Pregunta("Ciudad más poblada mundo?", "Tokio");
        preguntaRepository.insert(p6);
        Pregunta p7 = new Pregunta("¿Cuál es el río más largo del mundo?", "Nilo");
        preguntaRepository.insert(p7);
        Pregunta p8 = new Pregunta("¿Quién pintó “La última cena”?", "Leonardo da Vinci");
        preguntaRepository.insert(p8);
        Pregunta p9 = new Pregunta("¿Cuál es el océano más grande?", "Oceano Pacifico");
        preguntaRepository.insert(p9);
        Pregunta p10 = new Pregunta("¿Qué deporte practicaba Carl Lewis?", "atletismo");
        preguntaRepository.insert(p10);
        Pregunta p11 = new Pregunta(" ¿Cuántas patas tiene una araña?", "8");
        preguntaRepository.insert(p11);
        Pregunta p12 = new Pregunta("¿Cada cuántos años tenemos un año bisiesto?", "4");
        preguntaRepository.insert(p12);
        Pregunta p13 = new Pregunta("¿Qué planeta es el más cercano al Sol?", "Mercurio");
        preguntaRepository.insert(p13);
        Pregunta p14 = new Pregunta("¿Cuál es el país con mayor población del mundo?", "China");
        preguntaRepository.insert(p14);
        Pregunta p15 = new Pregunta("¿Cuántas sílabas tiene la palabra abecedario?", "5");
        preguntaRepository.insert(p15);
        Pregunta p16 = new Pregunta("¿Cuánto suman los ángulos de un triángulo?", "180");
        preguntaRepository.insert(p16);
        Pregunta p17 = new Pregunta("¿Cuánto suman los ángulos de un cuadrado?", "360");
        preguntaRepository.insert(p17);
        Pregunta p18 = new Pregunta("¿Cuántos kilos son una tonelada?", "1000");
        preguntaRepository.insert(p18);
        Pregunta p19 = new Pregunta("¿Cuál es la capital de Italia?", "roma");
        preguntaRepository.insert(p19);
        Pregunta p20 = new Pregunta("Infinito + infinito = ", "infinito");
        preguntaRepository.insert(p20);
    }

}


