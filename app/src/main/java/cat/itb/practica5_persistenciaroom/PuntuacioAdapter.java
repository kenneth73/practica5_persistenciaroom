package cat.itb.practica5_persistenciaroom;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.practica5_persistenciaroom.database.Puntuacio;

public class PuntuacioAdapter  extends RecyclerView.Adapter<PuntuacioViewHolder> {
    List<Puntuacio> scoresList;

    public PuntuacioAdapter(List<Puntuacio> scoresList) {
        this.scoresList = scoresList;
    }

    @NonNull
    @Override
    public PuntuacioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_puntuacio_list, parent, false);
        return new PuntuacioViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PuntuacioViewHolder holder, int position) {
        holder.bindData(this.scoresList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.scoresList.size();
    }
}

class PuntuacioViewHolder extends RecyclerView.ViewHolder{
    TextView nameTextView;
    TextView scoreTextView;

    public PuntuacioViewHolder(@NonNull View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.nameuser_text_view);
        scoreTextView = itemView.findViewById(R.id.score_text_view);
    }

    public void bindData(Puntuacio puntuacio) {
        nameTextView.setText(puntuacio.getUser_name());
        String puntuacioStr = puntuacio.getPuntuacio() + " punts";
        scoreTextView.setText(puntuacioStr);
    }
}