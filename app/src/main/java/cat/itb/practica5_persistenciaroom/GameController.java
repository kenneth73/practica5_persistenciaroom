package cat.itb.practica5_persistenciaroom;

import java.util.List;

import cat.itb.practica5_persistenciaroom.database.Pregunta;

public class GameController {
    public static final int TOTAL_QUESTIONS = 5;

    private String nameUser;
    private List<Pregunta> preguntes;
    private int currentIndex = 0;
    private float score;

    public GameController(String nameUser, List<Pregunta> preguntes) {
        this.nameUser = nameUser;
        this.preguntes = preguntes;
    }

    public Pregunta getCurrentQuestion() {
        return preguntes.get(currentIndex);
    }

    public void increaseIndex (){
        currentIndex++;
    }

    public void checkResponse(int index, String res) {
        if (preguntes.get(index).getResposta().equalsIgnoreCase(res)) {
            score +=1;
        }
    }

    public String getBreadCrumbs(){
        return (currentIndex+1)+ " de " + preguntes.size();
    }

    public float getScore() {
        return score;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setScore(float score) {
        this.score = score;
    }
}
