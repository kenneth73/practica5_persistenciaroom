package cat.itb.practica5_persistenciaroom.database;

import java.util.List;

public class PreguntaRepository {

    PreguntaDAO preguntaDao;

    public PreguntaRepository(PreguntaDAO preguntaDao) {
        this.preguntaDao = preguntaDao;
    }

    public List<Pregunta> getQuestions () {
        return preguntaDao.getQuestions();
    }

    public int getNumberQuestions() {
        return preguntaDao.getNumberQuestions();
    }

    public void insert(Pregunta p) {
        this.preguntaDao.insert(p);
    }

    /*public Pregunta findById(int id) {
        return preguntaDao.findById(id);
    }

    public void delete(Pregunta p) {
        this.preguntaDao.delete(p);
    }*/


}
