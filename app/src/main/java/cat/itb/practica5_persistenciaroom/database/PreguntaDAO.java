package cat.itb.practica5_persistenciaroom.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;


@Dao
public interface PreguntaDAO {

    @Insert
    void insert(Pregunta p);

    @Query("SELECT * FROM pregunta ORDER BY RANDOM() LIMIT 5")
    List<Pregunta> getQuestions();

    @Query("SELECT COUNT (*) FROM pregunta")
    int getNumberQuestions();

    /*@Delete
    void delete(Pregunta p);

    @Query("DELETE FROM pregunta")
    void deleteAll() ;

    @Query("SELECT * FROM pregunta WHERE id_pregunta=:id")
    Pregunta findById(int id);*/



}
