package cat.itb.practica5_persistenciaroom.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "pregunta")
public class Pregunta {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id_pregunta")
    private int id;
    private String enunciat;
    private String resposta;

    public Pregunta(String enunciat, String resposta) {
        this.enunciat = enunciat;
        this.resposta = resposta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnunciat() {
        return enunciat;
    }

    public void setEnunciat(String enunciat) {
        this.enunciat = enunciat;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
}
