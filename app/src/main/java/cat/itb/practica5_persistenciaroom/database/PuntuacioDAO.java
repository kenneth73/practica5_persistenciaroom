package cat.itb.practica5_persistenciaroom.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;

@Dao
public interface PuntuacioDAO {

    @Query("SELECT * FROM puntuacio ORDER BY puntuacio DESC")
    List<Puntuacio> getAll();

    @Insert
    void insert(Puntuacio p);

    @Query("DELETE FROM puntuacio")
    void deleteAll() ;

}
