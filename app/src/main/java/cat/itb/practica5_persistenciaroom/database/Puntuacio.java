package cat.itb.practica5_persistenciaroom.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "puntuacio")
public class Puntuacio {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_puntuacio")
    private int id;
    private String user_name;
    private float puntuacio;

    public Puntuacio(String user_name, float puntuacio) {
        this.user_name = user_name;
        this.puntuacio = puntuacio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public float getPuntuacio() {
        return puntuacio;
    }

    public void setPuntuacio(float puntuacio) {
        this.puntuacio = puntuacio;
    }
}
