package cat.itb.practica5_persistenciaroom.database;

import java.util.List;

public class PuntuacioRepository {
    PuntuacioDAO puntuacioDAO;

    public PuntuacioRepository(PuntuacioDAO puntuacioDAO) {
        this.puntuacioDAO = puntuacioDAO;
    }

    public List<Puntuacio> getAll() { return puntuacioDAO.getAll();}

    public void insert (Puntuacio p) {
        puntuacioDAO.insert(p);
    }

    public void deleteAll() {
        puntuacioDAO.deleteAll();
    }
}
