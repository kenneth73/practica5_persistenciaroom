package cat.itb.practica5_persistenciaroom;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cat.itb.practica5_persistenciaroom.database.AppDatabase;
import cat.itb.practica5_persistenciaroom.database.PuntuacioDAO;
import cat.itb.practica5_persistenciaroom.database.PuntuacioRepository;

public class MainActivity extends AppCompatActivity {
    static AppDatabase db;
    static PuntuacioDAO puntuacioDAO;
    static PuntuacioRepository puntuacioRepository;

    private TextView titleTextView;
    private Button playBtn;
    private TextView statsBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = AppDatabase.getInstance(this.getApplicationContext());
        puntuacioDAO = db.puntuacioDAO();
        puntuacioRepository = new PuntuacioRepository(puntuacioDAO);

        titleTextView = findViewById(R.id.title_text_view);
        playBtn = findViewById(R.id.play_btn);
        statsBtn = findViewById(R.id.stats_btn);
        titleTextView.setText("JOC DE PREGUNTES");


        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity1.class);
                startActivity(intent);
            }
        });

        statsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivityReciclerView.class);
                startActivity(intent);
            }
        });
    }

}
