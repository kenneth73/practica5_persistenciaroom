package cat.itb.practica5_persistenciaroom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cat.itb.practica5_persistenciaroom.database.AppDatabase;
import cat.itb.practica5_persistenciaroom.database.PuntuacioDAO;
import cat.itb.practica5_persistenciaroom.database.PuntuacioRepository;

public class MainActivity1 extends AppCompatActivity {

    private EditText nameEditText;
    private Button startBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_1);

        nameEditText = findViewById(R.id.name_edit_text);
        startBtn = findViewById(R.id.play_btn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nameEditText.getText().toString().isEmpty()){
                    String name = nameEditText.getText().toString();
                    Intent intent = new Intent(MainActivity1.this, MainActivity2.class);
                    intent.putExtra("name", name);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity1.this, "Your name is required for start game", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}